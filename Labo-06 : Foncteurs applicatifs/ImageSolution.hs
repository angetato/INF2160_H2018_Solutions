module Image where

import Control.Applicative

-----------
-- Types --
-----------

newtype Image a = Image { getImage :: [[a]] }
type Height = Int
type Width = Int
type Index = (Int,Int)

---------------
-- Functions --
---------------

-- | A string representation of an image.
--
-- >>> show $ makeImage 3 2 (const 1)
-- "[[1,1],[1,1],[1,1]]"
instance Show a => Show (Image a) where
    show i = (show . getImage) i

-- | The classical functor definition.
--
-- >>> fmap (1-) $ Image [[0,1,0],[1,1,0]]
-- [[1,0,1],[0,0,1]]
{- instance Functor Image where
    fmap f = Image . fmap (fmap f) . getImage -}
    
instance Functor Image where
    fmap f (Image i) = (Image . fmap (fmap f)) i

-- | A zip-like applicative definition.
--
-- >>> pure odd <*> Image [[0,0],[1,0]]
-- [[False,False],[True,False]]
instance Applicative Image where
    pure x = Image $ repeat (repeat x)
    Image fs <*> Image pxs = Image $ zipWith (zipWith id) fs pxs

-- | Returns an image inflated by a scaling factor
--
-- >>> makeImage 3 2 (const 1)
-- [[1,1],[1,1],[1,1]]
makeImage :: Height -> Width -> (Index -> a) -> Image a
makeImage h w f = Image [[f (i,j) | j <- [0..w-1]] | i <- [0..h-1]]

-- | Returns an image inflated by a scaling factor
--
-- >>> checkerImage 3 2 0 1
-- [[0,1],[1,0],[0,1]]
checkerImage :: Height -> Width -> a -> a -> Image a
checkerImage h w p1 p2 = Image $ take h $ cycle [oddLine, evenLine]
    where oddLine  = take w $ cycle [p1,p2]
          evenLine = take w $ cycle [p2,p1]

-- | Returns the pixel at the given position.
--
-- >>> getPixel (0,2) $ checkerImage 2 3 0 1
-- 0
getPixel :: Index -> Image a -> a
getPixel (i,j) image = getImage image !! i !! j

-- | Sets the pixel at the given position.
--
-- >>> setPixel (0,2) 1 $ checkerImage 2 3 0 1
-- [[0,1,1],[1,0,1]]
setPixel :: Index -> a -> Image a -> Image a
setPixel (i,j) v image@(Image pxs) = Image fs <*> image
    where fs = [[if (i,j) == (i',j') then const v else id
                | (j',colonne) <- zip [0..] ligne]
                | (i',ligne)   <- zip [0..] pxs]

-- | Adds two images
--
-- >>> addImages (checkerImage 2 3 0 1) (checkerImage 2 3 1 0)
-- [[1,1,1],[1,1,1]]
addImages :: Num a => Image a -> Image a -> Image a
addImages im1 im2 = (+) <$> im1 <*> im2

-- | Multiplies two images
--
-- >>> multImages (checkerImage 2 3 0 1) (checkerImage 2 3 1 0)
-- [[0,0,0],[0,0,0]]
multImages :: Num a => Image a -> Image a -> Image a
multImages im1 im2 = (*) <$> im1 <*> im2

-- | Returns an image inflated by a scaling factor
--
-- >>> checkerImage 2 3 0 1
-- [[0,1,0],[1,0,1]]
-- >>> inflate 2 $ checkerImage 2 3 0 1
-- [[0,0,1,1,0,0],[0,0,1,1,0,0],[1,1,0,0,1,1],[1,1,0,0,1,1]]
inflate :: Int -> Image a -> Image a
inflate s (Image pxs)
    | s <= 0    = error "Can only inflate with positive value"
    | otherwise = Image $ inflateList s . map (inflateList s) $ pxs
    where inflateList s = concatMap (replicate s)
