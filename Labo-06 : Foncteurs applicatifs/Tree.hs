module Tree where

import Control.Applicative

data Tree a = Empty | Leaf a | Branch a [Tree a]
    deriving Show

instance Functor Tree where
    fmap _ Empty = Empty
    fmap f (Leaf x) = Leaf (f x)
    fmap f (Branch x ts) = Branch (f x) (map (fmap f) ts)

instance Applicative Tree where
    pure x = Leaf x
    Empty       <*> _           = Empty
    _           <*> Empty       = Empty
    Leaf f      <*> Leaf x      = Leaf (f x)
    Leaf f      <*> Branch x _  = Leaf (f x)
    Branch f _  <*> Leaf x      = Leaf (f x)
    Branch f fs <*> Branch x xs = Branch (f x) [f' <*> x' | f' <- fs, x' <- xs]

tree1 = Branch 2 [Leaf 2,
                  Leaf 5]
tree2 = Branch 1 [Leaf 2,
                  Leaf 3,
                  Leaf 7]

tree3 = Branch 3 [Leaf 2,
                  Leaf 5,
                  Branch 7 [Leaf 5, Leaf 9]]
tree4 = Branch 2 [Leaf 2,
                  Branch 5 [Leaf 1],
                  Leaf 7,
                  Branch 7 [Leaf 5, Leaf 9]]
