# Laboratoire 11: Satisfiabilité et révision

## 1 - Casse-tête logique

Dans un examen à choix multiples, on vous propose la question suivante:

```
Quelle réponse est la bonne?

1. Toutes les réponses ci-bas.
2. Aucune réponse ci-bas.
3. Toutes les réponses ci-haut.
4. Au moins une réponse ci-haut.
5. Aucune réponse ci-haut.
6. Aucune réponse ci-haut.
```

En utilisant le solveur de contraintes CLPB de Prolog : 

```prolog
solution([A1,A2,A3,A4,A5,A6]) :-
        sat(A1 =:= A2*A3*A4*A5*A6),
        sat(A2 =:= ~(A3+A4+A5+A6)),
        sat(A3 =:= A1*A2),
        sat(A4 =:= card([1,2,3],[A1,A2,A3])),
        sat(A5 =:= ~(A1+A2+A3+A4)),
        sat(A6 =:= ~(A1+A2+A3+A4+A5)).
```

En allant from scratch :
```prolog
% je définie les opérateurs and et or qui m'aideront dans la défnition du prédicat solution.
:- op(900, fy,not).
:- op(810, yfx, and).
:- op(720, yfx, or).

and(A,B) :- A, B.

or(A,_) :- A.
or(_,B) :- B.

% Valeur(B), donnera à B la valeur true ou false.
valeur(true).
valeur(false).

% Vérifie que 2 atomes ont même valeur logique.
mm_valeur(X,Y):- X,Y.
mm_valeur(X,Y):- not(X),not(Y).

solution([A1,A2,A3,A4,A5,A6]) :-
        valeur(A1),valeur(A2),valeur(A3),valeur(A4),valeur(A5),valeur(A6),
        mm_valeur(A1,(A2 and A3 and A4 and A5 and A6)),
        mm_valeur(A2,(not(A2) and not(A3) and not(A4) and not(A5) and not(A6))),
        mm_valeur(A3, (A1 and A2)),
        mm_valeur(A4,(A1 or A2 or A3)),
        mm_valeur(A5,(not(A2) and not(A3) and not(A4) and not(A1))),
        mm_valeur(A6,(not(A2) and not(A3) and not(A4) and not(A1) and not(A5))),!.
```