# Laboratoire 3: Fonctions et type (Solution partielle)


# 3 - Jeu de tic-tac-toe

Pour cet exercice, vous devez récupérer le fichier
[TicTacToe.hs](https://gitlab.com/ablondin/inf2160-exercices/blob/master/exemples/TicTacToe.hs) disponible dans ce dépôt.

## 3.1 - Affichage d'une grille

Implémentez une fonction qui permet d'afficher une grille, dont la signature
est
```haskell
showGrille :: Grille -> String
```
qui affiche la grille sous format plus agréable.

Nous aimerions avoir le comportement suivant:
```haskell
Prelude> putStrLn $ showGrille ((X,O,Vide),(X,Vide,O),(X,Vide,Vide))
X | O |  
--+---+--
X |   | O
--+---+--
X |   |  
```
*Note*: la fonction `putStrLn` permet d'afficher une chaîne de caractère (donc
les guillemets n'y sont plus, et les `\n` sont interprétés comme des retours à
la ligne).


### Solution :
```haskell
showGrille :: Grille -> String
showGrille (a,b,c) = showGrille_ [a,b,c]
 
showGrille_ xs = intercalate separateurLigne (map showLines xs) -- on affiche ligne après ligne en intercalant le separateur de lignes

showLines (x,y,z) = showLines_ [x,y,z]
showLines_ xs = intercalate separateurCase (map showCase xs) -- on affiche chaque case mais on intercale le separateur de cases. Ex: X |   | O 

separateurLigne = "\n--+---+-- \n"
separateurCase = " | "

showCase x = if x == Vide then " " else show x  -- afficher une case qui est soit vide, soit contenant un coup

{- Remarque: Il est possible de l'écrire sans créer d'autres fonction en utilisant un where ou en mettant tout sur une même ligne.
J'ai choisis cette facon d'écrire pour que vous puissiez mieux comprendre... -}
```


## 3.2 - Réimplémentation de `jouerCoups`

Modifiez l'implémentation de la fonction `jouerCoups` pour qu'elle fasse appel
à la fonction `foldr` (ou `foldl`).

### Solution :
```haskell
jouerCoups_ l g = foldl(\acc (c,i,j) -> jouerCoup c (i,j) acc) g l
```


