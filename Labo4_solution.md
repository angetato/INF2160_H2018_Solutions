# Laboratoire 4: Types et foncteurs (Solution partielle)

# 1 - Types algébriques

Considérez le module [`Fruits`](Fruits.hs) (le fichier est disponible dans le
dépôt):

```haskell
module Fruits where

-- | Une couleur représentée par son niveau de rouge, vert et bleu
data Couleur = RGB Int Int Int

-- | Une image
data Image = Image {
    hauteur :: Int,
    largeur :: Int,
    pixels :: [[Couleur]]
}

-- | Indique si la hauteur et la largeur de l'image sont cohérentes avec la
-- matrice de pixels
estValide :: Image -> Bool
estValide = (Image h l p) = length (filter (\x-> length x  == l) p) == h

-- | Retourne une image de dimensions données dont tous les pixels sont de même
-- couleur
imageUnicolore :: Int     -- Hauteur
               -> Int     -- Largeur
               -> Couleur -- La couleur
               -> Image   -- L'image résultante
imageUnicolore h l c = Image h l pixels 
                       where
                          pixels = replicate h (replicate l c)

-- | Retourne une image de dimensions données en alternant les couleurs données
-- comme dans un échiquier.
echiquier :: Int     -- Hauteur
          -> Int     -- Largeur
          -> Couleur -- Première couleur
          -> Couleur -- Deuxième couleur
          -> Image   -- L'image résultante
echiquier h l c1 c2 = Image h l p
                      where p  = take h (cycle [lp, li])
                            lp = take l (cycle [c1, c2])
                            li = take l (cycle [c2, c1])

echiquierV2 h l c1 c2 = Image h l pixels  
                        where pixels = chunksOf l (take (h*l) (cycle [c1, c2]) ) -- nécessite l'importation du module Data.List.Split
```

