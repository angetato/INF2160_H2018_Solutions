# Solution Laboratoire 8: Introduction à Prolog

## 1 - Introduction

Considérez la base de faits décrite dans le fichier
[nourriture.pl](https://gitlab.com/ablondin/inf2160-exercices/blob/master/labo08/nourriture.pl) de ce dépôt. Lisez-le pour bien vous
familiariser avec son contenu.

Lancez ensuite l'interpréteur SWI-Prolog avec la commande
```
swipl nourriture.pl
```
Cela permet de charger les faits déclarés dans le fichier.

Interrogez cette base de connaissance pour avoir les informations suivantes:

1. Est-ce que le navet est sucré?
```prolog
gout(navet,sucre).
```
2. Quels sont les légumes déclarés dans la base?
```prolog
aliment(X,legume).
```
3. De quelle couleur est une pomme?
```prolog
couleur(pomme,X).
```
4. Quels sont les fruits sucrés?
```prolog
aliment(X,fuit), gout(X,sucre).
```
5. Quels sont les aliments de couleur orange et de goût sucré?
```prolog
aliment(X,_),couleur(X,orange),gout(X,sucre).
```
6. Quels légumes sont verts?
```prolog
aliment(X,legume),couleur(X,vert).
```

N'hésitez pas à modifier la base de faits pour effectuer d'autres tests. Notez
que pour recharger une base de fait, il suffit d'entrer (sans oublier le point
final):
```prolog
?- make.
```
Un exemple de modification de la base de faits :
```prolog
aliment(brocolis, legume).
couleur(brocolis, vert).
```
## 2 - Listes

Proposez une implémentation Prolog des prédicats suivants.

### 2.1 - `concat`

Définissez un prédicat `concat(L1, L2, L)`, qui indique si `L` est la liste
obtenue en concaténant `L1` et `L2`.

On s'attend donc au comportement suivant:

```prolog
concat([], L2, L2).
concat(L1, [], L1). % peut être enlevé mais permet de racourcir l'éxécution dans le cas où la 1ere liste n'est pas vide.
concat([X|XS], L2, [X|YS]):- concat(XS, L2, YS).
```

### 2.2 - `renverse`

Définissez un prédicat `renverse(L1, L2)`, qui indique si `L1` est la liste
obtenue de `L2` en renversant l'ordre dans lequel les éléments sont écrits.

*Indice*: N'hésitez pas à utiliser le prédicat `concat` défini plus haut.

```prolog
renverse([],[]).
renverse([X|XS], L):- renverse(XS,L1), concat(L1,[X], L),!.

%version 2 sans le concat
renverse2(L1,L2) :- renv(L1,[],L2).
renv([],L,L).
renv([X|L],L2,L3) :- renv(L,[X|L2],L3).
```

### 2.3 - `palindrome`

Définissez un prédicat `palindrome(L)`, qui indique si `L` est une liste
palindromique, c'est-à-dire que le premier élément est égal au dernier, le
deuxième à l'avant-dernier, etc.

*Indice*: N'hésitez pas à utiliser le prédicat `concat` défini plus haut.

On s'attend donc au comportement suivant:
```prolog
palindrome([]).
palindrome([_]).
palindrome(L):- concat([X|XS], [X], L), palindrome(XS),!.

%version 2
palindrome2(L) :- renverse(L,L).
```

## 3 - Modélisation

En géométrie de base, il existe différents types d'objets en 2D. Par exemple,

- Un *cercle*;
- Un *polygone*, qui a un nombre arbitraire de côtés (au moins 3);
- Un *triangle*, qui est un polygone de 3 côtés;
- Un *quadrilatère*, qui est un polygone de 4 côtés;

Parmi les quadrilatères, on peut raffiner le type également:

- Un *trapèze*, qui a au moins 1 paire de côtés parallèles;
- Un *parallélogramme*, qui a 2 paires de côtés parallèles;
- Un *rectangle*, qui a 4 angles droits;
- Un *losange*, qui a 4 côtés de même longueur et 2 paires de côtés parallèles;
- Un *carré*, qui a 4 côtés de même longueur et 4 angles droits;

Supposons que les atomes qui nous intéressent sont
```prolog
cercle
polygone
triangle
quadrilatere
trapeze
parallelogramme
rectangle
losange
carre
```
En utilisant des règles bien choisies, proposez un modèle pour ces différents
objets qui supporte les requêtes suivantes:
```prolog
est_cercle(X)
est_polygone(X)
est_triangle(X)
est_quadrilatere(X)
est_trapeze(X)
est_parallelogramme(X)
est_rectangle(X)
est_losange(X)
```
N'oubliez pas qu'un carré est un cas particulier de rectangle et de losange.
Par contre, un carré n'est pas un cercle.

Évidemment, il est possible de définir ces fonctions en faisant tous les cas
possibles, mais il existe une solution plus intéressante qui permet de faire de
l'inférence. Par exemple, on sait que si `X` est un rectangle, alors c'est
forcément un parallélogramme, un trapèze et un quadrilatère.

```prolog
est_cercle(cercle).

est_polygone(polygone).
est_polygone(X):- est_triangle(X).
est_polygone(X):- est_quadrilatere(X).

est_triangle(triangle).

est_quadrilatere(quadrilatere).
est_quadrilatere(X):- est_trapeze(X).

est_trapeze(trapeze).
est_trapeze(X):- est_parallelogramme(X).

est_parallelogramme(parallélogramme).
est_parallelogramme(X):- est_rectangle(X).
est_parallelogramme(X):- est_losange(X).

est_rectangle(rectangle).
est_rectangle(carre).

est_losange(losange).
est_losange(carre).

```
