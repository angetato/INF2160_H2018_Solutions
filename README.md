# Paradigmes de programmation

Ce dépôt contient les solutions *partielles* des laboratoires du cours INF2160 Paradigmes de
programmation, enseigné à l'UQAM pour l'hiver 2018. Pour d'autres exercices avec solutions, vizitez le site [gdac](http://gdac.uqam.ca/inf2160/) !

## Haskell
* [Labo 2](Labo2_solution.md)
* [Labo 3](Labo3_solution.md)
* [Labo 4](Labo4_solution.md)
* [Labo 6](https://gitlab.com/angetato/INF2160_H2018_Solutions/tree/master/Labo-06%20:%20Foncteurs%20applicatifs)

## Prolog
* [Labo 8](Labo8_solution.md)
* [Labo 9](Labo9)
* [Labo 10](Labo10)
* [Labo 11](Labo11_solution.md)

Commandes utiles dans prolog:
* `/* */`: pour des commentaires sur plusieurs lignes
* `%`: pour un commentaire sur une ligne
* `swipl fichier.pl` : pour éxécuter votre script fichier.pl à l'extérieur de l'invite prolog
* `['fichier.pl']`: pour éxécuter votre script fichier.pl une fois dans l'invite prolog
* `halt.`: pour quitter l'invite
